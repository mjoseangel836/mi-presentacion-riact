import React from 'react'
import './ComponenteDos.css'
import img from '../assets/yo.jpg'

function ComponenteDos() {
  return (
      <>
      <div className="containerDos">
    <div className="image">

        <img className="img" src={img} alt="" />
    </div>

    <div className="description">
        <h3>Nombre: Jose Angel Miranda Secundino</h3>
        <h3>Sexo: Masculino</h3>
        <h3>Carrera: Ingenieria en Sistemas Computacionales</h3>
        <h3>Semestre: 8vo</h3>
        <h3>Grupo: 3</h3>
        <h3>Materia: Servicios web</h3>
        <h3>Hobi: Tocar Guitarra</h3>
        <h3>Deporte: MMA y Kenpo Karate Americano</h3>
        <h3>ComidaFav: Salsa Verde con Carne</h3>
        <h3>MusicaFav: Las Rancheras, Corridos, Valadas, Tangos, etc.</h3>
    </div>
    
    

</div>
<br></br>
      </>
    
  )
}

export default ComponenteDos