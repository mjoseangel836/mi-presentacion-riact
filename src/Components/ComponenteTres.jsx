import React from 'react'
import './ComponenteTres.css'
import face from '../assets/facebook.png'
import what from '../assets/whatsapp.png'
import twit from '../assets/twitter.png'
import you from '../assets/youtube.png'
import git from '../assets/git.png'

function ComponenteTres() {
  return (
    <>

<div className='containerTres'>
    
<div className='face'>
    <a href='https://www.facebook.com/profile.php?id=100087340236421'><img className='imgFace' src={face} alt=""/></a>
    

    </div>
    <div className='whats'>
        <a href='https://wa.me/527351736617'><img className='imgWhat' src={what} alt=""/></a>
    

    </div>
    <div className='youtube'>
        <a href='https://www.youtube.com/channel/UC11bgQeE0gG7ZId_H6hZodw'><img className='imgYou' src={you} alt=""/></a>
    

    </div>
    <div className='twiter'>
        <a href='https://twitter.com/Josngel68537607'><img className='imgTwit' src={twit} alt=""/></a>
    

    </div>
    <div className='git'>
        <a href='https://gitlab.com/mjoseangel836'><img className='imgGit' src={git} alt=""/></a>
    

    </div>
</div>

    </>
    
    
  )
}

export default ComponenteTres